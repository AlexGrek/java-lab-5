/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.greko.labmessage;

import java.time.LocalDateTime;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author AlexG
 */
public class ModelTest {
    
    public ModelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of equals method, of class ForumMessage.
     */
    @Test
    public void testEqualsNull() {
        System.out.println("equals");
        Object obj = null;
        ForumMessage instance = new ForumMessage();
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
        /**
     * Test of equals method, of class ForumMessage.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");

        String s = "abc";
        LocalDateTime date = LocalDateTime.now();
        ForumMessage instance = new ForumMessage(s, s, s, date, date);
        ForumMessage obj = new ForumMessage(s, s, s, date, date);
        
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
            /**
     * Test of equals method, of class ForumMessage.
     */
    @Test
    public void testEqualsNot() {
        System.out.println("equals");

        String s = "abc";
        LocalDateTime date = LocalDateTime.now();
        ForumMessage instance = new ForumMessage(s, s, s, date, date);
        ForumMessage obj = new ForumMessage(s, s, "sfgsg", date, date);
        
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class ForumMessage.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        String s = "abc";
        LocalDateTime date = LocalDateTime.now();
        ForumMessage instance = new ForumMessage(s, s, s, date, date);
        ForumMessage instance2 = new ForumMessage(s, s, s, date, date);
        boolean expResult = true;
        boolean result = instance.hashCode() == instance2.hashCode();
        assertEquals(expResult, result);
    }
    
        /**
     * Test of hashCode method, of class ForumMessage.
     */
    @Test
    public void testHashCode2() {
        System.out.println("hashCode");
        String s = "abc";
        String s2 = "abdsgfc";
        LocalDateTime date = LocalDateTime.now();
        ForumMessage instance = new ForumMessage(s, s, s, date, date);
        ForumMessage instance2 = new ForumMessage(s, s2, s, date, date);
        boolean expResult = false;
        boolean result = instance.hashCode() == instance2.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of clone method, of class ForumMessage.
     */
    @Test
    public void testClone() {
        System.out.println("clone");
        String s = "aaa";
        LocalDateTime date = LocalDateTime.now();
        ForumMessage instance = new ForumMessage(s, s, s, date, date);
        ForumMessage expResult = new ForumMessage(s, s, s, date, date);
        ForumMessage result = instance.clone();
        assertEquals(expResult, result);
    }
    
}
