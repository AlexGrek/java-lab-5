/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.greko.labmessage;

import java.util.Scanner;

/**
 *
 * @author AlexG
 */
public class View {
    public static final String EDIT = "Edit message, enter nothing to leave it unchanged";
    public static final String WRONG_INPUT_DATA = "Wrong input, repeat please.";
    public static final String INPUT = "Enter ";
    public static final String ENTERED = "Entered data: ";
    public static final String ACTION = "[ 1 - show, 2 - edit, 3 - exit]";
    
    private final Scanner sc = new Scanner(System.in);
    
    public void printMessage(String message) {
        System.out.println(message);
    }
    
    public void promptInput(String message) {
        System.out.println(INPUT + message);
    }
    
    public void printField(String message, String data) {
        System.out.println(message + ": " + data);
    }
    
    public String readString() {
        while (!sc.hasNextLine()) {
            this.printMessage(View.WRONG_INPUT_DATA);
            sc.next();
        }
        return sc.nextLine();
    }
}
