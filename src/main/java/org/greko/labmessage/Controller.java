/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.greko.labmessage;

import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author AlexG
 */
public class Controller {
    ForumMessage model;
    View view;
    
    public Controller(ForumMessage mod, View vw) {
        model = mod;
        view = vw;
    }
    
    /**
     * initialization
     */
    public void create() {
        view.promptInput("username");
        model.setUser(readMatch("[A-Za-z0-9]+"));
        
        view.promptInput("topic");
        model.setTopic(readMatch(".*"));
        
        view.promptInput("text");
        model.setText(readMatch(".*"));
        
        model.setCreated(LocalDateTime.now());
        model.setModified(LocalDateTime.now());
    }
    
    /**
     * show fields
     */
    public void show() {
        view.printField("username", model.getUser());
        view.printField("topic", model.getTopic());
        view.printField("text", model.getText());
        view.printField("created", model.getCreated().toString());
        view.printField("modified", model.getModified().toString());
    }
    
    /**
     * modify fields
     */
    public void edit() {
        view.printField("username", model.getUser());
        String val = readMatchOrNull("[A-Za-z0-9]+");
        if (val != null)
            model.setUser(val);
        
        view.printField("topic", model.getTopic());
        val = readMatchOrNull(".*");
        if (val != null)      
            model.setTopic(val);
        
        view.printField("text", model.getText());
        val = readMatchOrNull(".*");
        if (val != null)
            model.setText(val);
        
        model.setModified(LocalDateTime.now());
    }
    
    /**
     *  processUser
     */
    public void loop() {
        for (;;) {
        view.printMessage(model.toString());
        view.printMessage(View.ACTION);
        int aid = Integer.parseInt(readMatch("[0-9]"));
        switch (aid) {
            case 1: 
                show();
                break;
            case 2: 
                edit();
                break;
            case 3: 
                System.exit(0);
            default:
                view.printMessage(View.ACTION);
                break;
        }
        }
    }
    
    /**
     * read value
     * @param pattern
     * @return string that matches pattern
     */
    public String readMatch(String pattern) {
        Pattern p = Pattern.compile(pattern);
        String input;
        do {
            input = view.readString();
            if (!matches(p, input))
                view.printMessage(View.WRONG_INPUT_DATA + " Pattern: " + pattern);
        } while (!matches(p, input));
        return input;
    }
    
    /**
     * read or return null
     * @param pattern
     * @return null or string that matches pattern
     */
    public String readMatchOrNull(String pattern) {
        Pattern p = Pattern.compile(pattern);
        String input;
        do {
            input = view.readString();
            if (input.isEmpty())
                return null;
            if (!matches(p, input))
                view.printMessage(View.WRONG_INPUT_DATA + " Pattern: " + pattern);
        } while (!matches(p, input));
        return input;
    }
    
    /**
     * regexp matcher
     * @param p pattern
     * @param str input
     * @return true if matches
     */
    public boolean matches(Pattern p, String str) {
        Matcher m;
        m = p.matcher(str);
        return m.matches();
    }
}
