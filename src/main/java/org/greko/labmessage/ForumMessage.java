/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.greko.labmessage;

import java.time.LocalDateTime;

/**
 *
 * @author AlexG
 */
public class ForumMessage extends Message {
    private String user;
    private String topic;
    private LocalDateTime created;
    private LocalDateTime modified;
    
    private String text;

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj)
            return true;
        if((obj == null) || (obj.getClass() != this.getClass()))
            return false;
        ForumMessage test = (ForumMessage)obj;
        return
            (user == null ? test.user == null : user.equals(test.user)) &&
            (topic == null ? test.topic == null : topic.equals(test.topic)) &&
            (getText() == null ? getText() == null : getText().equals(test.getText())) &&
            (modified == null ? test.modified == null : modified.equals(test.modified)) && 
            (created == null ? test.created == null : created.equals(test.created));
    }
    
    @Override
    public int hashCode()
    {
        int hash = user.hashCode() ^ topic.hashCode() ^ getText().hashCode() ^ 
                created.hashCode() ^ modified.hashCode();
        return hash;
    }
    
    @Override
    public ForumMessage clone() {
        ForumMessage cl = new ForumMessage(user, topic, getText(), created, modified);
        return cl;
    }
    
    public ForumMessage() {
        super();
    }
    
    public ForumMessage(String usr, String top, String txt) {
        text = txt;
        user = usr;
        topic = top;
        created = modified = LocalDateTime.now();
    }
    
    public ForumMessage(String usr, String top, String txt, LocalDateTime cr, LocalDateTime mod)  {
        text = txt;
        user = usr;
        topic = top;
        created = cr;
        modified = mod;
    }
    
    
    @Override
    public String toString() {
        return getUser() + " @ " + getTopic() + " |" + getModified() + "| " + getText();
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the topic
     */
    public String getTopic() {
        return topic;
    }

    /**
     * @param topic the topic to set
     */
    public void setTopic(String topic) {
        this.topic = topic;
    }

    /**
     * @return the created
     */
    public LocalDateTime getCreated() {
        return created;
    }

    /**
     * @param created the created to set
     */
    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    /**
     * @return the modified
     */
    public LocalDateTime getModified() {
        return modified;
    }

    /**
     * @param modified the modified to set
     */
    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    @Override
    public String getText() {
        return this.text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }
}
