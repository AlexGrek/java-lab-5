/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.greko.labmessage;

/**
 *
 * @author AlexG
 */
public class Main {
    
    public static void main(String[] args) {
    
        ForumMessage model = new ForumMessage();
        View view = new View();
        Controller controller = new Controller(model, view);
        controller.create();
        controller.show();
        controller.loop();
    }
}
