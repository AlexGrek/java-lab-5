/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.greko.labmessage;

/**
 *
 * @author AlexG
 */
public abstract class Message implements Cloneable {

    @Override
    public String toString() {
        return "Message{" + getText() + '}';
    }
    
     @Override
    public boolean equals(Object obj)
    {
        if(this == obj)
            return true;
        if((obj == null) || (obj.getClass() != this.getClass()))
            return false;
        Message test = (Message)obj;
        return
            (getText() == null ? getText() == null : getText().equals(test.getText()));
    }
    
    @Override
    public int hashCode()
    {
        int hash = getText().hashCode();
        return hash;
    }

            /**
     * @return the text
     */
    public abstract String getText();

    /**
     * @param text the text to set
     */
    public abstract void setText(String text);
}
