/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.greko.labmessage;

import java.util.Date;

/**
 *
 * @author AlexG
 */
public class ChatMessage extends Message {

    
     @Override
    public boolean equals(Object obj)
    {
        if(this == obj)
            return true;
        if((obj == null) || (obj.getClass() != this.getClass()))
            return false;
        ChatMessage test = (ChatMessage)obj;
        return
            (getText() == null ? getText() == null : getText().equals(test.getText())) &&
            (moment == null ? test.moment == null : moment.equals(test.moment));
    }
    
    @Override
    public int hashCode()
    {
        int hash = moment.hashCode() ^ getText().hashCode();
        return hash;
    }
    
    
    /**
     * @return the moment
     */
    public Date getMoment() {
        return moment;
    }

    /**
     * @param moment the moment to set
     */
    public void setMoment(Date moment) {
        this.moment = moment;
    }
    private Date moment;
    private String text;
    
    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }
    
}
